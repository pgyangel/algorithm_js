import lesson2 from '../../code/01-string/lesson2'

test('lesson2', () => {
  expect(lesson2("001010011"))
    .toBe(6)
})