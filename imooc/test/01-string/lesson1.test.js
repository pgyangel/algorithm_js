import lesson1 from '../../code/01-string/lesson1'

test('lesson1', () => {
  expect(lesson1("Let's take LeetCode contest"))
    .toBe("s'teL ekat edoCteeL tsetnoc")
})