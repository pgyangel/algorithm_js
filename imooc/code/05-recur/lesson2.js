/* 
与所有单词相关联的字符串

给定一个字符串s和一些长度相同的单词words。
找出s中恰好可以由words中所有单词串联形成的子串的起始位置。

注意子串要与words中的单词完全匹配，中间不能由其他字符，但不需要考虑words中单词串联的顺序。

示例1：
输入：
s = "barfoothefoobarman"
word = ["foo","bar"]
输出:[0,9]
解释：
从索引 0 和 9 开始的了串分别是 "barfoo" 和 "foobar"
输出的顺序不重要，[9,0]也是有效答案。
*/

export default () => {

}